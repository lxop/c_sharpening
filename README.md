# c_sharpening

A few simple projects (of increasing complexity) to get
to grips with C#.

Probably the following:

- Hello world
- Database entry (console interface)
- Database entry (GUI interface)
- Database entry (web interface)
