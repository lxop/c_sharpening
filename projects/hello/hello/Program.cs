﻿using System;


class Application
{
    static void Main()
    {
        // Get caller's name
        Console.Write("What's your name? ");
        var name = Console.ReadLine();
        // and give a more personalised greeting
        Console.WriteLine("Hello {0}!", name);
    }
}

