﻿using System;
using System.Data.SQLite;


namespace console_db
{
    class Database
    {
        // Use a constant for now, parametrise this later
        private const string db_file = "db.sqlite";

        public Database()
        {
            // Create DB file (should check for existence first...)
            SQLiteConnection.CreateFile(db_file);
            // Open database connection
            using var con = new SQLiteConnection($"Data Source={db_file};Version=3;");
            con.Open();

            // Check that schema is as expected
            using var command = new SQLiteCommand("SELECT SQLITE_VERSION();", con);
            string sqlite_version = command.ExecuteScalar().ToString();

            Console.WriteLine("Version is {0}", sqlite_version);
        }
    }
    class Program
    {
        static int Main()
        {
            // Connect to database
            var db = new Database();

            // Prompt for new database entry

            // Store new database entry

            return 0;
        }
    }
}
